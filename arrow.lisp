(in-package :ct)

;; Category
(defgeneric %id (tag)
  (:documentation "The identity function in the given category"))
(detag id)

(defgeneric %comp (tag a b)
  (:documentation "Composition of the given functions"))
(detag comp)
(defun <<< (a b) (comp a b))
(defun >>> (b a) (comp a b))

;; Arrow
(defgeneric %arr (tag fn)
  (:documentation "Lift a function."))
(detag arr)
(defun pure-A ()
  "The identity arrow"
  (arr #'identity))

(defgeneric %arr-split (tag a-car a-cdr)
  (:documentation "Apply A-CAR to the car and A-CDR to the CDR"))
(detag arr-split)

(defun arr-first (a-car)
  "Apply the arrow A-CAR only on the car of the input"
  (arr-split a-car (id)))
(defun arr-second (a-cdr)
  "Apply the arrow A-CDR only on the cdr of the input"
  (arr-split (id) a-cdr))
(defun arr-comb (a-car a-cdr &key (comb #'cons))
  "Split the input in two and combine the results when applied to A-CAR and A-CDR"
  (>>> (>>> (arr (lambda (x) (cons x x)))
            (arr-split a-car a-cdr))
        (arr (lambda (x) (funcall comb (car x) (cdr x))))))

(defun ^>> (fn a)
  "Compose a function with an arrow"
  (>>> (arr fn) a))
(defun >>^ (a fn)
  "Compose a function with an arrow"
  (>>> a (arr fn)))
(defun <<^ (a fn)
  "Compose a function with an arrow"
  (<<< (arr fn) a))
(defun ^<< (fn a)
  "Compose a function with an arrow"
  (<<< (arr fn) a))

;; TODO ArrowPlus

;; ArrowChoice
(defun union-tag (union) (cdr union))
(defun union-val (union) (car union))
(defun (setf union-val) (new-value union)
  (setf (car union) new-value))
(defun union-new (val tag) (cons val tag))

(defgeneric %arr-choice (tag a-left a-right)
  (:documentation "Run the arrow that corresponds to the correct tag A-LEFT is run if the tag is nil, and A-RIGHT if the tag is t"))
(detag arr-choice)

(defun arr-left (a)
  "Run the arrow only if the tag is nil."
  (arr-choice a (id)))
(defun arr-right (a)
  "Run the arrow only if the tag is t."
  (arr-choice (id) a))
(defun arr-fan (a-left a-right)
  "Run A-LEFT on a nil tag, A-RIGHT on a t tag, but return the value without the tag"
  (>>^ (arr-choice a-left a-right) #'union-val))

;; ArrowApply
(defgeneric %arr-app (tag)
  (:documentation "An arrow that runs the arrow given as an argument"))
(detag arr-app)

;; Arrow loop
(defgeneric %arr-loop (tag a)
  (:documentation "Feed back the cdr of the output as the cdr of the input (as a thunk)."))
(detag arr-loop)

;; Definition operators
(defmacro defcategory (tag id comp)
  "Define a new category compatible type tagged with a TAG."
  `(progn
     (def-inst-fmt %id ,tag ,id)
     (def-inst-fmt %comp ,tag ,comp)))
(defmacro defarrow (tag arr arr-split)
  "Define a new arrow compatible type tagged with a TAG."
  `(progn
     (def-inst-fmt %arr ,tag ,arr)
     (def-inst-fmt %arr-split ,tag ,arr-split)))

;;; Abstractions and macros
(defun do-a-get-dependencies (expression)
  "Get the dependencies of the expression"
  (cond
    ((and (symbolp expression) (equal #\@ (char (symbol-name expression) 0)))
     (list expression))
    ((listp expression)
     (remove-duplicates (concatenate 'list (mapcar #'do-a-get-dependencies expression))))
    (t nil)))
(defun do-a-lambda (vert &rest content)
  (let ((s (gensym))
        (vars (mapcar #'(lambda (x) (if x x (gensym))) vert)))
    `(lambda (,s)
       (destructuring-bind ,vars ,s
         (declare (ignorable ,@vars))
         ,@content))))
(defun do-a-parse-clauses (clauses)
  "Parse the arrow notation into components. Supports the following expressions

   [@bind-var <-] arrow -< (f @x)
   Which is equivalent to (^>> (lambda (x) (f x)) arrow)

   TODO [@bind-var <-] (arrow @x) -<< (f @x)
   Which is equivalent to (^>> (lambda (x) (cons ((arrow x), (f x)))) (arr-app))

   TODO (| |)

   TODO rec"
  (cond
    ((null clauses) nil)
    ((and (typep (cadr clauses) 'symbol)
          (equal "<-" (symbol-name (cadr clauses)))
          (typep (cadddr clauses) 'symbol)
          (equal "-<" (symbol-name (cadddr clauses))))
     (let* ((var (car clauses))
            (com (caddr clauses))
            (fn (car (cddddr clauses)))
            (dep (do-a-get-dependencies fn)))
       (cons
        (list var com fn dep)
        (do-a-parse-clauses (cdr (cddddr clauses))))))
    ((and (typep (cadr clauses) 'symbol)
          (equal "<-" (symbol-name (cadr clauses))))
     (let* ((com (car clauses))
            (fn (caddr clauses))
            (dep (do-a-get-dependencies fn)))
       (cons
        (list nil com fn dep)
        (do-a-parse-clauses (cdddr clauses)))))
    (t (error "Invalid arrow parse clauses: ~A" clauses))))
(defun do-a-solve (vert arrow clause following)
  "Bind the next block in the chain. This performs a trivial conversion.
VERT are the currently bound variables and their position in the chain.
ARROW is the current arrow.
CLAUSE is the clause that is currently trying to be added at the end.
FOLLOWING are the clauses that follow this clause.
The values returned are the new arrow and the second value is the new vert."
  (declare (ignore following))
  ;; Check that every variables are there
  (destructuring-bind (var com fn dep) clause
    ;; (unless (every #'(lambda (var) (member var vert)) dep) (error "Some variables bound by the clause ~A are not in scope" clause))
    (values
     `(>>> ,arrow
           (^>>
            ;; Duplicate
            #'dup
            ;; Apply the clause on the left side of the cons
            (arr-first
             (^>>
              ;; Run the expression
              ,(do-a-lambda vert fn)
              ;; Eval the variable
              ,com))))
     (cons var vert))))
(defmacro let-a (invar (&rest clauses) &body ret)
  "Bind and chain arrow operations."
  (let ((content
          (reduce #'(lambda (arrowvar clause)
                      ;; TODO Pass along following
                      (multiple-value-list (do-a-solve (second arrowvar) (first arrowvar) clause nil)))
                  (do-a-parse-clauses clauses)
                  :initial-value `((arr (lambda (x) (list x))) (,invar))
                  )))
    `(>>^
      ,(first content)
      ,(apply #'do-a-lambda (second content) ret))))


;;; Common instances

;; :identity
;; Objects are basic functions with an arity of 1
(defcategory :identity
  ( () #'identity )
  ( (a b) (lambda (x) ($ a ($ b x)) )))
(defarrow :identity
  ( (x) x )
  ( (a-car a-cdr) (lambda (x) (cons (funcall a-car (car x))
                                    (funcall a-cdr (cdr x)))) ))
(def-inst-fmt %arr-choice :identity
  ( (a-left a-right) (lambda (u) (if (union-tag u)
                                     (union-new (funcall a-right (union-val u)) (union-tag u))
                                     (union-new (funcall a-left (union-val u)) (union-tag u))))))
(def-inst-fmt %arr-app :identity
  ( () (lambda (f v) (funcall f v)) ))
(def-inst-fmt %arr-loop :identity
  ( (fn) (lambda (b) (labels ((get-d () (cdr (funcall fn (cons b #'get-d)))))
                       (seq (car (funcall fn (cons b #'get-d)))))) ))
