(in-package :ct)

(defun arglist (fn)
  "Get the arglist of the function FN."
  ;; TODO for non-sbcl
  (sb-introspect:function-lambda-list fn))
(defun arity (fn)
  "Get the arity of the function FN."
  (let ((arglist (arglist fn)))
    (if (intersection arglist lambda-list-keywords)
        (error "~S lambda list ~S contains keywords" fn arglist)
        (length arglist))))

(defun $ (fn a)
  "Call a function with a single argument. If the function takes more than one argument, this will return a function that take one less argument.
For instance, ($ ($ f a) b) will return 12 for any of the following function f:
  (lambda (a b) 12)
  (lambda (a) (lambda (b) 12))
  (lambda () (lambda (a) (lambda () (lambda (b) 12))))
"
  (macrolet (( curried-lambda (n)
               (let ((syms (loop for i below n collect (gensym))))
                 `(lambda ,syms (funcall fn a ,@syms)))))
    (cond
      ((not (typep fn 'function)) (error "Can't call ~A which is not a function" fn))
      ((= 0 (arity fn)) ($ (funcall fn) a))
      ((= 1 (arity fn)) (funcall fn a))
      ((= 2 (arity fn)) (curried-lambda 1))
      ((= 3 (arity fn)) (curried-lambda 2))
      ((= 4 (arity fn)) (curried-lambda 3))
      ((= 5 (arity fn)) (curried-lambda 4))
      (t (error "$ Can't do function with over ~A arguments" 3)))))


(defun const (x y)
  "This function returns its first argument and ignores the second."
  (declare (ignore y))
  x)

(defun dup (x) (cons x x))
