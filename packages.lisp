(defpackage :cl-ct
  (:use :cl)
  (:nicknames :ct)
  (:export
   ;; Thunks
   #:seq
   #:defun!
   ;; Monad
   #:const
   #:$

   #:fmap
   #:<$
   #:let-m
   #:defmonad
   #:defalternative
   #:defapplicative
   #:deffunctor
   #:bind
   #:with-inst
   #:pure
   #:liftA2
   #:<*>
   #:<*
   #:*>
   #:liftA3
   #:empty
   #:alt
   #:alt-many
   #:alt-some
   #:>>
   ;; Parser
   #:parser-trap-signal
   #:parser-run
   #:parser-alt
   #:parser-trap
   #:parser-try
   #:parser-fail
   #:parser-maybe
   #:parser-eof
   #:parser-many
   ))
