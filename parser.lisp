(in-package :ct)

;; Helper
(defmacro or-by (test &body tests)
  (reduce #'(lambda (clause acc)
              (let ((s (gensym)))
                `(let ((,s ,clause))
                   (if (funcall ,test ,s) ,s ,acc))))
          tests
          :from-end t
          :initial-value nil))

;; Parser
;; The object is a cons cell containing the value along with a flag used to break execution when a parser fails.
;; The bind operation

(defvar *current-tokens-list* nil
  "This is the list of tokens currently being parsed")

(define-condition parser-trap-signal (error)
  ((trap-status :initform nil :accessor parser-trap-status))
  (:documentation "Base class for a parser error. This error type
 allows the parser to abort when encountering a critical error or to backup and consider alternatives."))

(defmonad :parser
  ( (x) (cons x t) )
  ( (x next) (when (cdr x) (funcall next (car x))) ))

;; Basic tokens types
(defgeneric parser-token-empty (tokens)
  (:documentation "Method that checks if the token list is empty"))
(defmethod parser-token-empty ((tokens list)) (null tokens))

(defgeneric parser-token-peek (tokens)
  (:documentation "Method that returns the next token without removing it"))
(defmethod parser-token-peek ((tokens list)) (car tokens))
(defmethod parser-token-peek ((tokens string)) (if (< 0 (length tokens)) (char tokens 0) nil))

(defgeneric parser-token-next (tokens)
  (:documentation "Method that pop the next token"))
(defmethod parser-token-next ((tokens t)) (parser-token-peek tokens))
(defmethod parser-token-next :after ((tokens list)) (pop *current-tokens-list*))
(defmethod parser-token-next :after ((tokens string)) (unless (string= "" tokens) (setf *current-tokens-list* (subseq *current-tokens-list* 1))))

;; Token manipulation
(defmacro parser-run (tokens &body parser)
  "Execute the PARSER on the given list of TOKENS."
  `(with-inst :parser
    (let ((*current-tokens-list* ,tokens))
      (values (progn ,@parser) *current-tokens-list*))))
(defmacro parser-eof ()
  "This parser fail if there are more tokens"
  (if (null *current-tokens-list*)
      (%pure :parser t)
      (parser-fail)))
(defun parser-next ()
  "Parser that returns the next token."
  (let ((tok (parser-token-next *current-tokens-list*)))
    (if tok
        (%pure :parser tok)
        (parser-fail))))
(defun parser-peek ()
  "Parser that return the next token, but does not parse it."
  (let ((tok (parser-token-peek *current-tokens-list*)))
    (if tok
        (%pure :parser tok)
        (parser-fail))))

;; Basic parser entities

(defmacro parser-look (action)
  "Parser that perform the given action, but restores the token list afterwards. Make the parser not consume any input."
  (let* ((temp (gensym))
         (result (gensym)))
    `(let ((,temp *current-tokens-list*)
           (,result (parser-try ,action)))
       (setf *current-tokens-list* ,temp)
       ,result)))
(defmacro parser-alt (&body clauses)
  "Consider multiple alternatives. Returns the first one that fully succeeds."
  (if clauses
      (let ((res (gensym)))
        `(with-inst :parser
           (let ((,res (or-by #'cdr
                        ,@(loop for c in clauses
                                collect `(parser-try ,c)))))
             (if (cdr ,res)
                 ,res
                 (parser-fail)))))
      `(parser-fail)))
(defmacro parser-trap (action)
  "Mark any errors so they can't be ignored by `parser-try'."
  `(handler-case ,action (parser-trap-signal (e) (setf (parser-trap-status e) t) (error e))))
(defmacro parser-try (action)
  "Try to perform the action and abort calculation on any error"
  (let ((temp (gensym)))
    `(let* ((,temp *current-tokens-list*))
       (handler-case ,action
         (parser-trap-signal (e)
           (when (parser-trap-status e) (error e))
           (setf *current-tokens-list* ,temp)
           (cons nil nil))))))
(defun parser-fail ()
  "Parser action that always fail"
  (error 'parser-trap-signal))
(defun parser-maybe (action &optional ret)
  "Try the parser, but do not abort compilation if it failed, return RET instead"
  (let ((parser-value (parser-try action)))
    (if (cdr parser-value)
        (%pure :parser (car parser-value))
        (%pure :parser ret))))
;; TODO Add endby
(defmacro parser-many (action &key (separator nil separator-p) max)
  "Perform the parser action maximum MAX times (or as many time as possible by default), with a minimum of MIN times, with possibly a separator between each action."
  `(loop for action-value = (parse-try ,action)
         ,@(if max `(for i below ,max))
         ,@(if separator-p `(for separator-value = (parse-try ,separator)
                                 and last-separator-value = t then (cdr separator-value)))
         while ,(if separator-p `(and action-value last-separator-value) 'action-value)
         collect action-value))

;; Higher level combinators

(defun-m parser-tok :parser (el &key (test #'eq))
  "Parse a token matching the given element"
  tok <- (parser-next)
  (if (funcall test tok el)
      (pure tok)
      (parser-fail)))
(defun-m parser-oneof :parser (cases &key (test #'eq))
  "A parser that matches a token only if that token is one of the given CASES under TEST."
  tok <- (parser-next)
  (if (member tok (coerce cases 'list) :test test)
      (pure tok)
      (parser-fail)))
(defun-m parser-noneof :parser (cases &key (test #'eq))
  "A parser that matches a token only if that token is not one of the given CASES under TEST."
  tok <- (parser-next)
  (if (member tok (coerce cases 'list) :test test)
      (parser-fail)
      (pure tok)))
(defun parser-count (number action)
  "Run the parser ACTION NUMBER times."
  (if (> number 0)
      (%let-m :parser (a <- action
                       as <- (parser-count (- number 1) action))
        (cons a as))))
(defun-m parser-between :parser (!open !close !action)
    "Run the parser OPEN before and the parser CLOSE after the given action"
  !open
  act <- !action
  !close
  (pure act))
(defun parser-chain (parser chain &key from-end (initial-value 0 initial-value-p))
  "Parse the parser PARSER one or more times. If INITIAL-VALUE is provided, the parser can be parsed zero times,
INITIAL-VALUE being the result in that case.
The parser CHAIN must return a function of arity 2 which is used to combine the results of PARSER. This composition is left-associative
unless FROM-END is provided and is non-nil, in which case, the composition is right associative."
  (if initial-value-p
      (parser-alt
        (parser-chain parser chain :from-end from-end)
        (pure initial-value))
      (if from-end
          ;; Right-assoc version
          (labels ((start-parse ()
                     (bind parser #'cont-parse))
                   (cont-parse (a)
                     (parser-alt
                       (let-m (f <- (funcall chain)
                               b <- (start-parse))
                         ($ ($ f a) b))
                       (pure a))))
            (start-parse))
          ;; Left-assoc version
          (labels ((cont-parse (a)
                     (parser-alt
                       (do-m
                         f <- (funcall chain)
                         b <- (funcall parser)
                         (cont-parse ($ ($ f a) b)))
                       (pure a))))
            (%bind :parser parser #'cont-parse)))))

;; TODO Error when the operators are ambiguous
(defmacro parser-build-operator-expression (ops &body action)
  "Parse an expression composed of terms ACTION along with operators as described by the OPS structure.
The OPS structure is a list of list of operators. Each inner list contain operators with the same fixity.
The outer list contains the operators in descending fixity.
Each operator is composed of a cons cell, a type tag that is either :PRE :POST :BIN-L :BIN-R or :BIN; a parser
 action that parse the operator symbol and return a function of two elements"
  (labels ((split-ops (op lst) (destructuring-bind (rasc lasc nasc pre post) lst
                                  (destructuring-bind (type . fun) op
                                    (cond
                                      ((eq :pre type) (list rasc lasc nasc (cons fun pre) post))
                                      ((eq :post type) (list rasc lasc nasc pre (cons fun post)))
                                      ((and (eq :bin-l type)) (list rasc (cons fun lasc) nasc pre post))
                                      ((and (eq :bin-r type)) (list (cons fun rasc) lasc nasc pre post))
                                      ((and (eq :bin type)) (list rasc lasc (cons fun nasc) pre post))
                                      (t (error "Invalid operator description"))))))
           (call-operator (op) op)
           ( make-parser (term ops)
             (destructuring-bind (rasc lasc nasc pre post)
                 (reduce #'split-ops ops :from-end t :initial-value '(() () () () ()))
               `(labels (
                         (term-parser ()
                            (%let-m :parser
                                (pre <- (parser-alt ,@(mapcar #'call-operator pre) (pure #'identity))
                                 x <- ,term
                                 post <- (parser-alt ,@(mapcar #'call-operator post) (pure #'identity)))
                              ($ post ($ pre x))))
                          (binop-rassoc (x)
                            (%let-m :parser
                                (right <- (parser-alt ,@(mapcar #'call-operator rasc))
                                 y <- (bind (term-parser) (lambda (z) (parser-alt (binop-rassoc z) (pure z)))))
                              ($ ($ right x) y)))
                          (binop-lassoc (x)
                            (parser-alt
                              (do-m
                                left <- (parser-alt ,@(mapcar #'call-operator lasc))
                                y <- (term-parser)
                                (binop-lassoc ($ ($ left x) y)))
                              (pure x)))
                          (binop-nassoc (x)
                            (%let-m :parser
                                (non <- (parser-alt ,@(mapcar #'call-operator nasc))
                                 y <- (term-parser))
                              ($ ($ non x) y))))
                  (%do-m :parser
                    x <- (term-parser)
                    (parser-alt
                      (binop-rassoc x)
                      (binop-lassoc x)
                      (binop-nassoc x)
                      (pure x)))))))
    (reduce #'make-parser ops :initial-value `(progn ,@action))))


;; String specific parsers
(defun parser-s-number ()
  "Parse an integer with parse-integer"
  (multiple-value-bind (ret n) (parse-integer *current-tokens-list* :junk-allowed t)
    (if ret
        (progn (setf *current-tokens-list* (subseq *current-tokens-list* n)) (%pure :parser ret))
        (parser-fail))))
