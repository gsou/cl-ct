(in-package :ct)

;; Functions and helpers to manage thunks and lazy execution

(defun traverse-list (f l)
  (funcall f (if (listp l)
                 (mapcar (lambda (x) (traverse-list f x)) l)
                 l)))
(defun seq (f &key tree)
  "Fully evaluate thunk."
  (cond
    ((and (functionp f) (ignore-errors (= 0 (arity f)))) (seq (funcall f)))
    ((and tree (listp f)) (mapcar (lambda (x) (seq x :tree t)) f))
    (t f)))

;; TODO Support &optional
(defun defun!-gen-key-element (as val)
  (apply #'concatenate 'list
         (loop for a in as
               collect (cond
                         ( (and (symbolp a) (eq #\! (char (symbol-name a) 0)))
                           (let ((s (intern (symbol-name a) :keyword)))
                             `(,s (lambda nil ,(getf val s)))) )
                         ( (and (listp a) (eq #\! (char (symbol-name (car a)) 0)))
                           (let ((s (intern (symbol-name (car a)) :keyword)))
                           `(,s (lambda nil ,(or (getf val s) (cadr a)))) ))
                         ( (symbolp a)
                           (let ((s (intern (symbol-name a) :keyword)))
                           (list s (getf val s)) ))
                         ( (listp a)
                           (let ((s (intern (symbol-name (car a)) :keyword)))
                           (list s (or (getf val s (cadr a))))))
                         ( t (error "Can't parse the arglist for defun!") )))))
(defun defun!-gen-element (a val)
    (cond
      ( (null a) nil )
      ( (null val) nil )
      ( t (let ((x (car a))
                (xs (cdr a)))
            (cond
              ( (and (symbolp x) (string= "&OPTIONAL" (symbol-name x))) (defun!-gen-element xs val) )
              ( (and (symbolp x) (string= "&KEY" (symbol-name x))) (defun!-gen-key-element xs val) )
              ( (and (symbolp x) (eq #\! (char (symbol-name x) 0)))
                (concatenate 'list
                             (list `(lambda nil ,(car val)))
                             (defun!-gen-element xs (cdr val))) )
              ( (listp x) (concatenate 'list (list (car val)) (defun!-gen-element xs (cdr val))))
              ( t (concatenate 'list (list (car val)) (defun!-gen-element xs (cdr val)))))))))
(defmacro defun!-call (name args argvalues)
  `(,name ,@(defun!-gen-element args argvalues)))

(defmacro defun! (name arglist &body clauses)
  "Wrap the arguments starting with `!' with a thunk in the arglist.
The function is defined with a ending '!' and a macro without the ending '!' is defined with the thunk making feature."
  (labels ( (conv-arglist (x) (cond
                                ((and (listp x) (eq :! (car x))) (cadr x))
                                ((and (symbolp x) (eq #\! (char (symbol-name x) 0))) (intern (subseq (symbol-name x) 1)) )
                                (t x)))
            (get-arglist (arg)  (mapcar #'conv-arglist arg))
            (strip! (name) (intern (subseq (symbol-name name) 1)))
            (autoseq (l) (cond
                           ( (and (symbolp l) (eq #\! (char (symbol-name l) 0))) `(seq ,(strip! l)))
                           ( t l ))))
      (let ((name-! (intern (concatenate 'string (symbol-name name) "!"))))
        `(progn
           (defun ,name-! ,(get-arglist arglist) ,@(traverse-list #'autoseq clauses))
           (defmacro ,name (&rest args)
             ,@(if (and clauses (stringp (car clauses))) (list (car clauses)))
             ,(list 'list ''defun!-call (list 'quote name-!) (list 'quote arglist) 'args) )))))

(defmacro defun-strict (name old)
  "Strict version of the given command"
  (let ((s (gensym)))
    `(defun ,name (&rest ,s)
       (seq (apply (function ,old) (seq ,s :tree t))))))

;; Lazy lists

(defparameter *lcons-max-print-length* 10 "Maximum amount of elements to print by default for a lcons list")
(defclass lcons ()
  ((lcar :initarg :lcar
         :accessor lcons-car)
   (lcdr :initarg :lcdr
         :accessor lcons-cdr)))

(defun! lcons (!a !d) "Build a cons cell" (make-instance 'lcons :lcar a :lcdr d))
(defun-strict lcar lcons-car)
(defun-strict lcdr lcons-cdr)

(defun ltake (lst n)
  "Take N elements from the lazy list and evaluate them.
   The second value is t if the lst has been completely used and the third value is the
   last value of the list."
  (if (<= n 0)
      (values nil (eq nil (seq lst)) nil)
      (if (typep lst 'lcons)
          (multiple-value-bind (tak end last) (ltake (lcdr lst) (- n 1))
              (values (cons (lcar lst) tak) end last))
          (values nil t lst))))

(defmethod print-object ((obj lcons) stream)
  (multiple-value-bind (tak end last) (ltake obj *lcons-max-print-length*)
      (format stream "#l(~{~a~^ ~}~@[ . ~a~]~:[ ...~;~])" tak last end)))

(defun lmapcar (function list)
  (lcons (funcall function (lcar list))
         (lambda () (lmapcar function (lcdr list)))))
