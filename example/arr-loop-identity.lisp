
;; Test of arr-loop

(defun power-loop (x)
  "A factorial function that uses loop to compute. D can't be evaluated outside of a lambda because it will trigger infinite recursion."
  (let ((input (car x))
        (d (cdr x)))
    (cons d (lcons input (lmapcar #'(lambda (y) (* input y)) d) ))))
(defun powers-of (x)
  "An infinite list of the powers of x"
  ($ (%arr-loop :identity #'power-loop) x))
