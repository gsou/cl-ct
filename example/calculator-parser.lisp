;; This example shows a simple parser that parses and compute simple mathematical expressions

(defun calculator-make-op (symbol &optional (tag (symbol-name symbol)))
  `(:bin (lambda () (<$ (lambda (a b) (,symbol a b)) (parser-oneof ,tag :test #'equal)))))
(defun calculator-parser-exp ()
  "Parse a term from the calulator."
  (parser-alt
   (parser-between
      (parser-tok #\()
      (parser-tok #\))
      (calculator-parser))
   (parser-s-number)))
(defun calculator-parser ()
  "Parse a mathematical expression."
  (parser-build-operator-expression
      ((#.(calculator-make-op 'expt "^"))
       (#.(calculator-make-op '*)
        #.(calculator-make-op '/))
       (#.(calculator-make-op '+)
        #.(calculator-make-op '-)))
    (calculator-parser-exp)))
