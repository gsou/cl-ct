(in-package :ct)

;; Tag functions
(defparameter *debug-trace-instance-change* nil
  "Trace changes in instance inside of the macros `lift', `with-inst' and `trans'")
(defvar *current-implicit-instance* nil
  "The currently used instance. This dynamic variable is used in the tagless version of the
   functions below. This variable shound not be accessed directly.")
(defmacro with-inst (tag &body body)
  "Execute the following functions within the context of the given tag"
  `(let ((*current-implicit-instance*  (if (listp ,tag) ,tag (list ,tag))))
     (when *debug-trace-instance-change*
       (format t "~&WITH_INST ~A: ~A" ,tag (quote ,@ body)))
     ,@body))
;; XXX Check that the first arg is indeed the tag
(defmacro detag (fn)
  (let* ((tagf (intern (format nil "%~A" (symbol-name fn))))
         (al (arglist tagf)))
    `(defun ,fn ,(cdr al)
       (funcall (function ,tagf) (car *current-implicit-instance*) ,@(cdr al)))))
;; Transformers
(defmacro lift (&body body)
  "Run computation in a lower instance"
  `(let ((*current-implicit-instance* (cdr *current-implicit-instance*)))
     (when *debug-trace-instance-change*
       (format t "~&LIFT [~A]: ~A" *current-implicit-instance* (quote ,@ body)))
    ,@body))
(defmacro trans (tag &body body)
  "Run computations in a higher instance"
  `(let ((*current-implicit-instance* (cons ,tag *current-implicit-instance*)))
     (when *debug-trace-instance-change*
       (format t "~&TRANS [~A] ~A: ~A" *current-implicit-instance* ,tag (quote ,@ body)))
     ,@body))
(defmacro lambda-m (name arglist &body body)
  "Generate a computation locked in the current context."
  (let ((capture-implicit-instance (gensym)))
    `(let ((,capture-implicit-instance *current-implicit-instance*))
       (labels ((,name ,arglist (with-inst ,capture-implicit-instance
                                  ,@body)))
         (function ,name)))))


;;; Functor
(defgeneric %fmap (tag f obj)
  (:documentation "Special map that acts differently on different kinds of object"))
(detag fmap)
(defun <$ (a obj)
  (fmap (lambda (x) (declare (ignore x)) a) obj))

;;; Applicative
(defgeneric %pure (tag a)
  (:documentation "Lift a pure value into the context of the tag"))
(detag pure)
(defgeneric %liftA2 (tag f obj1 obj2)
  (:documentation "Apply a function over two objects"))
(detag liftA2)

(defun <*> (a b)
  (liftA2 #'identity a b))
(defun <* (a b)
  (liftA2 #'const a b))
(defun *> (a b)
  (<*> (<$ #'identity a) b))
(defun liftA3 (f a b c)
  (<*> (liftA2 f a b) c))

(defun %fmap-default-a (tag f obj)
  (%liftA2 tag #'$ (%pure tag f) obj))

;; Alternative
(defgeneric %empty (tag)
  (:documentation "The identity under the alternative operator"))
(detag empty)
(defgeneric %alt (tag a !b-thunk)
  (:documentation "Alternative. The second argument is passed as a thunk to allow lazy execution."))
(detag alt)

;;(defun alt-many (a)
;;  (alt (alt-some a) (pure nil)))
;;(defun alt-some (a)
;;  (<*> (fmap #'cons a) (alt-many a)))


;; Monad
(defgeneric %bind (tag obj next))
(detag bind)

(defun >> (obj next)
  (bind obj (lambda (x) (declare (ignore x)) next)))

(defun %fmap-default-m (tag f obj)
  "Default implementation of %fmap for an object that has `pure' and `bind' defined."
  (%bind tag obj (lambda (a) (%pure tag ($ f a)))))
(defun %liftA2-default-m (tag f obj1 obj2)
  "Default implementation of %liftA2 for an object that has `pure' and `bind' defined."
  (%bind tag obj1
         (lambda (a) (%bind tag obj2
                            (lambda (b) (%pure tag ($ ($ f a) b)))))))

;; MonadFix
(defgeneric %mfix (tag f))
(detag mfix)


;; Definitions operators

(defmacro def-inst-fmt (name tag fmt)
  `(defmethod ,name ,(cons `(tag (eql ,tag)) (car fmt)) ,@(cdr fmt)))

(defmacro deffunctor (tag fmap)
  "Define a new functor compatible type tagged with a TAG. %fmap must be specified in the following format
  ( (function object) code )."
  `(def-inst-fmt %fmap ,tag ,fmap))
(defmacro defapplicative (tag pure liftA2)
  "Define a new applicative compatible type tagged with a TAG."
  `(progn
     (defmethod %fmap ((tag (eql ,tag)) f obj) (%fmap-default-a tag f obj))
     (def-inst-fmt %pure ,tag ,pure)
     (def-inst-fmt %liftA2 ,tag ,liftA2)))
(defmacro defmonad (tag pure bind)
  "Define a new monad compatible type tagged with a TAG."
  `(progn
     (defmethod %fmap ((tag (eql ,tag)) f obj) (%fmap-default-m tag f obj))
     (defmethod %liftA2 ((tag (eql ,tag)) f obj1 obj2) (%lifta2-default-m tag f obj1 obj2))
     (def-inst-fmt %pure ,tag ,pure)
     (def-inst-fmt %bind ,tag ,bind)))

;; TODO Autogenerate thunk for alt
(defmacro defalternative (tag empty alt)
  "Define a new alternative compatible type tagged with a TAG."
  `(progn
     (def-inst-fmt %empty ,tag ,empty)
     (def-inst-fmt %alt ,tag ,alt)))

;; Abstractions and macros

(defun do-m-parse-clauses (clauses)
  (cond
    ((null clauses) nil)
    ((and (typep (cadr clauses) 'symbol) (equal "<-" (symbol-name (cadr clauses))))
     (cons (list (car clauses) (caddr clauses)) (do-m-parse-clauses (cdddr clauses))))
    (t (cons (list :_  (car clauses)) (do-m-parse-clauses (cdr clauses))))))
(defmacro let-m ((&rest clauses) &body ret)
  "Bind and chain operations in a monad. Example:
   (let-m ( var1 (monad-producing-val)
            (monad-producing-val-not-binded) )
     (do-something-with var1))"
  (reduce #'(lambda (clause acc) (destructuring-bind (var exp) clause
                                   (if acc
                                       (if (eq :_ var)
                                           `(>> ,exp ,acc)
                                           `(bind ,exp #'(lambda (,var) (declare (ignorable ,var)) ,acc)))
                                       exp)))
          (do-m-parse-clauses clauses)
          :from-end t
          :initial-value (if ret `(pure (progn ,@ret)))))
(defmacro %let-m (tag (&rest clauses) &body ret)
  `(with-inst ,tag (let-m (,@clauses) ,@ret)))
(defmacro %do-m (tag &body clauses)
  `(%let-m ,tag ,clauses))
(defmacro do-m (&body clauses)
  `(let-m ,clauses))

(defmacro defun-m (name tag arglist documentation-or-clause &body body)
  "Generate a function with the body being inside of a `let-m' block."
  (let ((documentation (if (stringp documentation-or-clause) (list documentation-or-clause)))
        (clauses (if (stringp documentation-or-clause) body (cons documentation-or-clause body))))
    `(defun! ,name ,arglist
      ,@documentation
      (%let-m ,tag ,clauses))))


;; Basic instances

;; :identity
;; The alternative uses nil as the null element. It is equivalent to `or', but strict in its arguments.
(defmonad :identity
  ( (x) x )
  ( (x next) (funcall next x) ))
(defalternative :identity
  ( () nil )
  ( (a b) (or a (funcall b)) ))
(def-inst-fmt %mfix :identity
    ( (f) (labels ((x () ($ f #'x)))  #'x) ))


;; :list
;; The alternative returns the first non nil list
(defmonad :list
  ( (x) (list x) )
  ( (x next) (apply #'concatenate 'list (mapcar next x)) ))
(defalternative :list
  ( () nil )
  ( (a b) (or a (funcall b)) ))

