(asdf:defsystem #:cl-ct
  :description "A collection of little tools and abstractions."
  :version "0.1"
  :author "gsou"
  :license "MIT"
  :components ((:file "packages")
               (:file "common" :depends-on ("packages"))
               (:file "thunk" :depends-on ("common"))
               (:file "monad" :depends-on ("thunk"))
               (:file "parser" :depends-on ("monad"))
               (:file "arrow" :depends-on ("monad"))))
